<?php
function find()
{
    $search=0;
    $val=0;
    $data=$_SESSION['FileText'];
    $occurrences = substr_count(strtolower($data), strtolower($_POST["find"]));
    $search=stripos($_SESSION['FileText'],$_POST["find"],$search);
    if ($search !== false) {
        for($i=1;$i<=$occurrences;$i++){
            $val=stripos($data,$_SESSION['FileFind'],$val+1);
            if($_SESSION['counter']<=$occurrences && $i==$_SESSION['counter']){
                if(isset($_POST["replace"]) && isset($_POST['set'])){
                    $part1=substr($data, 0,$val);
                    $_SESSION['lan2']=$_SESSION['lan1']+strlen($_POST["replace"]);
                    $data1=preg_replace('/'.$_SESSION['FileFind'].'/i',$_POST["replace"],$data,$i);
                    $val=$val+($i-1)*(strlen($_POST['replace'])-strlen($_SESSION['FileFind']));
                    $part2=substr($data1,$val);
                    $data=$part1.$part2;
                    return $data;
                }
                $part1=substr($data, 0,$val);
                $_SESSION["lan1"]=strlen(str_replace("\n", "", $part1));;
                $part2 = substr($data,$val);
                $data=$part1.$part2;
                $_SESSION['lan2']=$_SESSION['lan1']+strlen($_POST["find"]);
                return $data;
            }elseif($_SESSION['counter']>$occurrences){
                echo '<script type="text/javascript">alert("No More ' . $_POST['find'] . ' Found");</script>';
                return $data;
            }
        }
    }else{
        echo '<script type="text/javascript">alert("' . $_POST['find'] . ' Not Found");</script>';
        return $data;
    }
}
?>