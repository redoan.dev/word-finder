<?Php
    session_start();
    include_once "functions.php";
    if(isset($_POST['reset'])){
        session_destroy();
    }
    if(isset($_POST["save"])){
        if(isset($_SESSION['FileReplace'])&&isset($_SESSION['FileFind'])){
        file_put_contents($_SESSION['FileData'],$_SESSION['data']);
        }
    }
    if(isset($_POST['submit'])){
    if(isset($_POST["file"]) && $_POST["file"]!=""){
        $_SESSION['FileData'] =$_POST["file"];
    $_SESSION['counter'] = 0;
    }
}
    if(isset($_POST['text']) && $_POST["text"]!=""){
        $data=$_SESSION['FileText'];
    }else{
        $_SESSION['FileText']=file_get_contents($_SESSION['FileData']);
        $data=$_SESSION['FileText'];
    }
    if(!isset($_SESSION['counter'])) {
        $_SESSION['counter'] = 0;
    }
    if(isset($_POST['next'])){
    if(isset($_POST["find"]) && $_POST["find"]!=""){
        if($_POST['find']!=$_SESSION['FileFind']){
            $_SESSION['counter'] = 0;
        }
        ++$_SESSION['counter'];
        $FileFind= $_POST["find"];
        $_SESSION['FileFind']=$FileFind;
        $_SESSION['find']=$_POST['find'];
        $data=find();
        $lan1=$_SESSION["lan1"];
        $lan2=$_SESSION['lan2'];
    }
}
if(isset($_POST['set'])){
    if(isset($_POST["replace"]) && $_POST["replace"]!=""){
        $FileReplace= $_POST["replace"];
        $data=find();
        $_SESSION['data']=$data;
        $_SESSION['FileReplace']=$FileReplace;
        $_SESSION['FileReplace']=$_POST["replace"];
        $lan1=$_SESSION["lan1"];
        $lan2=$_SESSION['lan2'];
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Word finder</title>
        <link rel="stylesheet" href="assests/css.css">
        <link rel="stylesheet" href="assests/normalize.css">
        <link rel="stylesheet" href="assests/milligram.css">
        <style>
            textarea {
                min-height: 40rem;
            }
        </style>
        <script src=assests/jquery.min.js id=jquery-core-js></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="column column-60 column-offset-20" style="text-align: center;">
                    <h1>Find your word below</h1>
                    <p>Enter tour text to find</p>
                </div>
            </div>
            <div class="row">
                <div class="column">
                <form method="POST" action="index.php" enctype="multipart/form-data">
                    <label for="file"><em>Enter file name with full path</em></label>
                    <input type="text" name="file" id="file" value="<?php echo $_SESSION['FileData'];?>">
                    <input type="submit" name="submit">
                    <label for="find"><em>Find text</em></label>
                    <input type="text" name="find" id="find" value="<?php echo $_SESSION['FileFind'];?>">
                    
                    <button type="submit" name="next" id="insert">Find Next</button>
                    <input type = "hidden" name = "counter" value = "<?php $_SESSION['counter']; ?>";>
                    <label for="replace"><em>Replace Word</em></label>
                    <input type="text" name="replace" id="replace" value="<?php echo $_SESSION['FileReplace'];?>">
                    <input type="submit" name="set" value="Replace">
                    <button type="submit" name="save"><em>Save</em></button>
                    <button type="submit" name="reset"><em>Reset</em></button>
                    </form>
                </div>
                <div class="column" style="text-align: center; height:500px">
                    <textarea type="text" name="text" id="text"><?php echo $data;?></textarea>
                </div>
            </div>
        </div>
        <script>
        //function getCaretPosition(ctrl) {
    // IE < 9 Support 
//     if (document.selection) {
//         ctrl.focus();
//         var range = document.selection.createRange();
//         var rangelen = range.text.length;
//         range.moveStart('character', -ctrl.value.length);
//         var start = range.text.length - rangelen;
//         return {
//             'start': start,
//             'end': start + rangelen
//         };
//     } // IE >=9 and other browsers
//     else if (ctrl.selectionStart || ctrl.selectionStart == '0') {
//         return {
//             'start': ctrl.selectionStart,
//             'end': ctrl.selectionEnd
//         };
//     } else {
//         return {
//             'start': 0,
//             'end': 0
//         };
//     }
// }

function setCaretPosition(ctrl, start, end) {
    // IE >= 9 and other browsers
    if (ctrl.setSelectionRange) {
        ctrl.focus();
        ctrl.setSelectionRange(start, end);
    }
    // IE < 9 
    else if (ctrl.createTextRange) {
        var range = ctrl.createTextRange();
        range.collapse(true);
        range.moveEnd('character', end);
        range.moveStart('character', start);
        range.select();
    }
}
// Credits: http://blog.vishalon.net/index.php/javascript-getting-and-setting-caret-position-in-textarea/
// function setCaretPosition(ctrl, pos) {
//   // Modern browsers
//   if (ctrl.setSelectionRange) {
//     ctrl.focus();
//     ctrl.setSelectionRange(pos, pos);
  
//   // IE8 and below
//   } else if (ctrl.createTextRange) {
//     var range = ctrl.createTextRange();
//     range.collapse(true);
//     range.moveEnd('character', pos);
//     range.moveStart('character', pos);
//     range.select();
//   }
// }

// Set the cursor position of the "#test-input" element to the end when the page loads
var input = document.getElementById('text');
setCaretPosition(input, <?php echo $lan1; ?>,<?php echo $lan2; ?>);
</script>
</body>
</html>
<?php 
//   if(isset($_POST['reset'])){
//       session_destroy(); 
//   }
?>